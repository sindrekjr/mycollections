using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MyCollections
{
    class ChaosArray<T> : IEnumerable<T>
    {
        // The actual collection
        (T item, bool occupied)[] array;
        int count = 0;

        // Public property holding the array's max capacity
        public int Capacity { get; set; }

        // Constructor. Capacity is here defaulted to 100
        public ChaosArray(int capacity = 100)
        {
            Capacity = capacity;
            array = new (T, bool)[Capacity];
        }

        public bool Insert(T item)
        {
            if(count >= Capacity) throw new AtMaxCapacityException();

            int randomIndex = new Random().Next(0, Capacity);

            if(array[randomIndex].occupied)
            {
                //return Insert(item);
                throw new IndexOccupiedException();
            }
            else
            {
                array[randomIndex] = (item, true);
                count++;
                return true;
            }
        }

        public T Get() => array[new Random().Next(0, Capacity)].item ?? Get();

        public T Remove()
        {
            int randomIndex = new Random().Next(0, Capacity);

            if(count <= 0 || !array[randomIndex].occupied)
            {
                //return Remove();
                throw new EmptyException();
            }
            else
            {
                T item = array[randomIndex].item;
                array[randomIndex] = (default(T), false);
                count--;
                return item;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            T[] results = array.Where(e => e.occupied).Select(e => e.item).ToArray();

            Random rand = new Random();
            foreach(T item in results.OrderBy(i => rand.Next())) yield return item;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    class AtMaxCapacityException : Exception { }
    class IndexOccupiedException : Exception { }
    class EmptyException : Exception { }
}