﻿using System;

namespace MyCollections
{
    class Program
    {
        const int ArraySize = 5000;

        static ChaosArray<int> ChaosArray;
        
        static void Main(string[] args)
        {
            ChaosArray = new ChaosArray<int>(ArraySize);
            string failedToAdd = "Failed to add:\n";
            for(int i = 0; i < ArraySize; i++)
            {
                if(!AddToChaosArray(i)) failedToAdd += $"{i}, ";
            }

            Console.WriteLine(failedToAdd.Split(',').Length);

            // The below foreach can be used to get every element from the chaos array in a random order
            //foreach(int i in ChaosArray) Console.WriteLine(i);

            // The below code can be used to get a random element from the chaos array
            //Console.WriteLine(ChaosArray.Get());
        }

        static bool AddToChaosArray(int element)
        {
            try
            {
                return ChaosArray.Insert(element);
            }
            catch(IndexOccupiedException)
            {
                return false;
                // The following is extremely likely to cause stack overflow because we're
                // making new attempts recursively
                //AddToChaosArray(element);
            }
        }
    }
}
